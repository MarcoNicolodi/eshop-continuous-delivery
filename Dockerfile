# RUN ALL CONTAINERS FROM ROOT (folder with .sln file):
# docker-compose build
# docker-compose up
#
# RUN JUST THIS CONTAINER FROM ROOT (folder with .sln file):
# docker build --pull -t web -f src/Web/Dockerfile .
#
# RUN COMMAND
#  docker run --name eshopweb --rm -it -p 5106:5106 web
FROM microsoft/dotnet:2.2-sdk AS base
WORKDIR /app
COPY . .
ENV ASPNETCORE_ENVIRONMENT=Development
RUN dotnet restore

FROM base as unittests
WORKDIR /app/tests/UnitTests
ENTRYPOINT [ "dotnet", "test" ]

FROM base as integrationtests
WORKDIR /app/tests/IntegrationTests
ENTRYPOINT [ "dotnet", "test" ]

FROM base as functionaltests
WORKDIR /app/tests/FunctionalTests
ENTRYPOINT [ "dotnet", "test" ]

FROM base as build
WORKDIR /app/src/Web
RUN dotnet publish Web.csproj --no-restore -c Release -o /app/out

FROM microsoft/dotnet:2.2-aspnetcore-runtime AS runtime
WORKDIR /app
ENV ASPNETCORE_ENVIRONMENT=Development
EXPOSE 80
COPY --from=build /app/out ./
ENTRYPOINT ["dotnet", "Web.dll"]

# Optional: Set this here if not setting it from docker-compose.yml
# ENV ASPNETCORE_ENVIRONMENT Development

